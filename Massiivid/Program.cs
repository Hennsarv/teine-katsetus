﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 3, 2, 4, 5, 7, 6, 8, 9, 0 };

            int[][] miskijora = {
                new int[]{ 1, 2, 3 },
                new int[]{ 1, 2 },
                new int[] { 4, 7, 8 }
            };

            int[,] tabel =  { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } } ;

            Console.WriteLine(miskijora[0][2]);
            Console.WriteLine(tabel[1,1]);

            int[] tyhi = new int[0];

            Console.WriteLine(args.Length); // argumentide massiivi (enamasti 0)

            Console.WriteLine(tabel.Length); // kõigis mõõtmetes kokku
            Console.WriteLine(miskijora[0].Length); // konkreetse elemendi pikkus

            Console.WriteLine(tabel.Rank);
            Console.WriteLine(tabel.GetLength(0));

        }
    }
}
