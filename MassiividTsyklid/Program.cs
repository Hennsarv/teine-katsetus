﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiividTsyklid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hommik! Neljapäeva hommik :)");

            string vastus;
            int summa = 0;

            List<int> arvud = new List<int>();

            do
            {
                Console.Write("Anna arv: ");
                vastus = Console.ReadLine();
                if (int.TryParse(vastus, out int i))
                {
                    summa += i;
                    arvud.Add(i);
                }
                else if (vastus != "") Console.WriteLine($"see \"{vastus}\" pole miski arv");
            } while (vastus != "");
            Console.WriteLine("sa andsid mulle sellised arvud");
            
            for (int l = 0; l < arvud.Count; ++l)
            {
                Console.Write($"{arvud[l]}\t");
                if (l % 8 == 0) Console.WriteLine();
            }
            Console.WriteLine($"\nkokku oli neid {arvud.Count} tükki");

            Console.WriteLine($"arvude summa on {summa:0000}");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            var test = new List<int>(100); // siin ma annan algse suuruse ette
            Console.WriteLine($"testi suurus on {test.Count} ja maht on {test.Capacity}" );
            for (int i = 0; i < 10; i++)
            {
                test.Add(i);
            }
            Console.WriteLine("peale 10 liikme lisamist");
            Console.WriteLine($"testi suurus on {test.Count} ja maht on {test.Capacity}");
            for (int i = 0; i < 100; i++)
            {
                test.Add(i);
            }
            Console.WriteLine("peale veel 100 liikme lisamist");
            Console.WriteLine($"testi suurus on {test.Count} ja maht on {test.Capacity}");

            


        }
    }
}
