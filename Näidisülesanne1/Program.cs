﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Näidisülesanne1
{
    class Program
    {
 

        static void Main(string[] args)
        {
            //int i = 77;

            Console.Title = "Paarisarvu kontrollimise programm";

            Console.Write("Anna üks arv: ");
            if (int.TryParse(Console.ReadLine(), out int i))
            {

                #region esimene variant
                // esimene variant a) tingimuslausega
                if (i % 2 == 0)
                {
                    Console.WriteLine($"a) ee {i} on paarisarv");
                }
                else
                {
                    Console.WriteLine($"a) see {i} on paaritu arv");
                }

                #endregion

                #region teine variant
                // teine variant b) tingimusavaldisega

                Console.WriteLine($"b) see {i} on {(i % 2 == 0 ? "paaris" : "paaritu ")}arv");

                #endregion
            }
            else Console.WriteLine("see pole üldse arv");

            
            
            
        }

        
    }
}
