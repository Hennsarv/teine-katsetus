﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineKatsetus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Alustame!");


            int a, b, c;
            c = (a = 3) + (b = 4);
            Console.WriteLine("a={0} b = {1} c={2}", a,b,c);

            c += 2;   // c = c + 2;
            a *= 3;
            b -= 1;



            int i = 5;
            Console.WriteLine(i); // 5
//            Console.WriteLine(++i); // trükitakse i uus väärtus (6)
            Console.WriteLine(i++); // trükitakse i vana väärtus (5)
            Console.WriteLine(i); // 6

            int õlut = 7;
            int joojaid = 3;
            Console.WriteLine( õlut / joojaid ); // 2 - igale kaks pudelit
            Console.WriteLine( õlut % joojaid ); // 1 - järele jääb 1

            // kõikvõimlaikud võrdlustehted
            // nende väärtuseks on jah/ei - true v. false


            int x = 6;
            int y = 5;

            Console.WriteLine(6 & 5);
            Console.WriteLine(6 | 5);

            i = int.MaxValue;

            Console.WriteLine(i + 7);
            double d = 77e-1;
            Console.WriteLine(d);

            // natukene stringidest 
            // (ei mitte need riided, mis nööridest tehtud)

            string eesNimi = "Henn";
            string pereNimi = "Sarv";
            string nimi = eesNimi + " " + pereNimi;

            string tootenimi1 = @"Liköör ""Vana Tallinn""  ";
            string tootenimi2 = "Liköör \"Vana Tallinn\"  ";

            Console.WriteLine(tootenimi1);
            Console.WriteLine(tootenimi2);

            string folderiNimi = "C:\\Users\\sarvi\\Videos\\Lync Recordings";
            string teineFolder = @"C:\Users\sarvi\Videos\Lync Recordings";

            Console.WriteLine( folderiNimi == teineFolder); // true


            Console.WriteLine("See jutt\non kahel real\b jutt");

            Console.WriteLine("Henn\ton\tilus\tpoiss");

            DateTime dt = new DateTime(2018, 6, 1);

            DateTime d2 = DateTime.Parse("1. september");
            Console.WriteLine(d2.AddDays(-300));

            DateTime dn = DateTime.Now; // tänane
            DateTime dh = DateTime.Parse("7. märts 1955"); // sünnipäev
            Console.WriteLine((dn - dh).TotalHours); // vanus päevades

            Console.WriteLine(dh.AddDays(25000));

            var imelik = 7 + 4;
            Console.WriteLine(imelik.GetType().FullName);

            var vanus = (dn.Year - dh.Year);

            string amet = vanus > 63 ? "pensionär" : "töötu";

            // lisaks

            string sVanus = vanus.ToString();
            string s77 = (70 + 7).ToString();

            int i77 = int.Parse(s77);

        }
    }


}
