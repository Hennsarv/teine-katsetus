﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valgusfoor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Mis värv seal on? ");
            string värv = Console.ReadLine().ToLower();

            //if (värv == "punane" || värv == "red")
            //{
            //    Console.WriteLine("jää seisma");
            //}
            //else if (värv == "roheline" || värv == "green")
            //{
            //    Console.WriteLine("võid sõita");
            //}
            //else if (värv == "kollane" || värv == "yellow")
            //{
            //    Console.WriteLine("vaata ette");
            //}
            //else
            //{
            //    Console.WriteLine("see valgusfoor on rikkis");
            //}

            // sama switchiga

            switch (värv)
            {
                case "punane":
                case "red":
                    Console.WriteLine("jää seisma");
                    break;
                case "roheline":
                case "green":
                    Console.WriteLine("võid sõita");
                    break;
                case "kollane":
                case "yellow":
                    Console.WriteLine("vaata ette");
                    break;
                default:
                    Console.WriteLine("see valgusfoor on rikkis");
                    break;
            }
        }
    }
}
