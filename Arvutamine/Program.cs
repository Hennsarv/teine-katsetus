﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arvutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Anna üks arv: ");
            string vastus = Console.ReadLine();
            
            //int iVastus = int.Parse(vastus); // kui string ei ole arvu kujul, siis tekib viga

            if (int.TryParse(vastus, out int iVastus))
            {

                Console.WriteLine("sinu arvu {0} ruut on {1}", iVastus, iVastus * iVastus);
                Console.WriteLine($"sinu arvu {iVastus} ruut on {iVastus * iVastus}");
            }
            else
            {
                Console.WriteLine("see pole miski arv");
            }



        }
    }
}
