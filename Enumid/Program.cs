﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumid
{
    enum Mast { Risti=0, Ruutu, Ärtu, Poti}

    enum Kaart { Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}

    [Flags]
    enum Tunnus { Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}


    class Program
    {
        static void Main(string[] args)
        {

                  


            string[] MastiNimed = { "Risti", "Ruutu", "Ärtu", "Poti" };
            string[] KaardiNimed = { "Kaks", "Kolm", "Neli", "Viis", "Kuus", "Seitse", "Kaheksa", "Üheksa", "Kümme", "Soldat", "Emand", "Kuningas", "Äss" };

            var Kaardid = Enumerable.Range(0, 52).ToList();
            foreach(var k in Kaardid)
                Console.WriteLine($"Kaart\t{k}\t on {(Mast)((k/13)%4)} {(Kaart)(k%13)}");

            Console.WriteLine("ilma segamata oleks:");
            Console.WriteLine("Noral\t\t\tEndlil\t\t\tSohvil\t\t\tWillul");
            Console.WriteLine(new string('-',100));
            foreach(var k in Kaardid)
                Console.Write($"{ (Mast)((k / 13) % 4)} { (Kaart)(k % 13)}\t\t"+ (k%4==3?"\n":""));

            Random r = new Random();
            var Segatud = new List<int>();
            while (Kaardid.Count > 0)
            {
                int mitmes = r.Next() % Kaardid.Count;
                Segatud.Add(Kaardid[mitmes]);
                Kaardid.RemoveAt(mitmes);
            }
            Kaardid = Segatud;
            Console.WriteLine("peale segamist on:");
            Console.WriteLine("Noral\t\tEndlil\t\t\tSohvil\t\t\tWillul");
            Console.WriteLine(new string('-', 100));
            for (int i = 0; i < Kaardid.Count; i++)
            {
                int k = Kaardid[i];
                Console.Write($"{ (Mast)((k / 13) % 4)} { (Kaart)(k % 13)}\t\t" + (i % 4 == 3 ? "\n" : ""));
            }

            SortedSet<int>[] Pakid = { new SortedSet<int>(), new SortedSet<int>(), new SortedSet<int>(), new SortedSet<int>() };
            for (int i = 0; i < 13; i++)
            for (int j = 0; j < 4; j++)
            {
                int mitmes = r.Next() % Kaardid.Count;
                Pakid[j].Add(Kaardid[mitmes]);
                Kaardid.RemoveAt(mitmes);
            }
            Console.WriteLine(new string('-',100));
            Console.WriteLine("Sorditud pakid oleks mul sellised");
            for (int i = 12; i >= 0; i--)
            {
                for (int j = 0; j < 4; j++)
                {
                    int k = Pakid[j].ToArray()[i];
                    Console.Write($"{ (Mast)((k / 13) % 4)} { (Kaart)(k % 13)}\t\t");
                }
                Console.WriteLine();
            }

        }


    }
}
