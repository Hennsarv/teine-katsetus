﻿namespace Enumid
{
    public class Raamat
    {
        public string Pealkiri;
        public int IlmumisAasta;
        public Kirjastus Kirjatus;

    }

    public class Kirjastus
    {
        public string Linn;
        public string Nimi;
        public Inimene Omanik;
    }

    public class Inimene
    {
    }
}